# The IRC Anti-Klin (AK)

This project is a reaction by Jacob 'Kreyren' Hrbek to his klin (IRC network bans) from the freenode network that he believes is corrosive to the user freedom and abusive for reasons below and thus giving everyone the option to practice their freedom.

```
I've been klined 12/12/2020 by freenode/staff/jess for allegedly harrasing Mysia (that the OP failed to proof) in #anime that is (to my knowledge) not supported by #anime ops nor members in the channel.

Prior to this i've been banned in #debian and #bash by greycat after around 8 months of hate speech by this invidual agains me that is allegedly due to invidual's homophobia.

Before that I've been banned for 1 year from ##linux by sauvin for using sms-lang when none was requesting support during a chat with other community members after which i requested the ban to be permanent and informed other FSF members about this behavior which caused most of them to do the same.

Thus i've created this project for everyone to be unbannable and unklinable to force freenode and other networks to maintain a healty culture of it's users and adapt measures to not be easily manipulated into klins that can happen to everyone.
```

## Footprint
### Freenode is using DroneBL Database
Thus this project requires OpenProxies not logged in this database which turns out is not difficult to accomplish, but their status needs to be checked as they are moslty offline.

### NickServ requires an e-mail verification
In addition to this some networks like Freenode has allowlisted list of domains from which they allow creation of an accounts.

Turns out there are services providing disposable e-mails on services like Gmail which are implemented in the source code.

## Abstract
### Breaking in Freenode
1. Generate a list of usable SOCKS5 proxies that are not logged in DroneBL database.
- Optionally create a VPN of community members providing closed proxies to access the network.

2. In addition to the used SOCKS5 proxy route the traffic over TOR.

3. Create a database of verified user accounts with `SASL EXTERNAL` to allow login over TOR without the need of non-tor proxy.

4. Create a plugin that changes your identity on demand by using the user account from the database
- These accounts has to be created over-time to make it harder for freenode to auto-klin
